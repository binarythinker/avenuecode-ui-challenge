define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var ageModel = Backbone.Model.extend({
		initialize: function() {
			this.getMinAge();
		},
		defaults: {
			ageMin: 18,
			ageMax: 90
		},
		getMinAge: function() {
			return this.get('ageMin');
		},
		getMaxAge: function() {
			return this.get('ageMax');
		},
		getMinMax: function() {
			return "The new values are: " + this.get('ageMin') + " " + this.get('ageMax');
		},
		validate: function() {
			if((attrs.age < 18) || (attrs.age > 90)) { 
				return "Age must be between the ages of 18 and 90";
			}
		},
		convertMinYear: function() {
			var d = new Date();
			return year_born = d.getFullYear() - this.getMinAge() ;
		},
		convertMaxYear: function() {
			var d = new Date();
			return year_born = (d.getFullYear() - this.getMaxAge());
		}
	});
	return ageModel;
});
