define(['jquery', 'underscore', 'backbone', 'currentView', 'formView', 'collection', 'model'], 
	function($, _, Backbone, currentView, formView, collection, model) {
    var collectionView = Backbone.View.extend({
        initialize: function() {
            this.collection = new collection;
            this.fetch();
        },

        fetch: function() {
            this.listenTo(this.collection, "reset", this.render);
            this.collection.fetch({reset: true});
            this.render();
        },

        render: function() {
            this.collection.each(function(item) {
                this.renderModel(item);
            }, this);
        },

        renderModel: function(item) {
            new currentView ({
                model: item,
            });
            new formView ({
                model: item,
            });
        }
    });
    return collectionView;
});
