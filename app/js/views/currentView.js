define(['jquery', 'underscore', 'backbone', 'navView'], function($, _, Backbone, NavView) {
	var currentView = NavView.extend({
		el: "#searchResults",
		template: _.template($("#current-template").html())
	});
	return currentView;
});
