define(['jquery', 'underscore', 'backbone', 'model'], 
	function($, _, Backbone, model) {
		return navView = Backbone.View.extend({
			initialize: function() {
				var self = this;
				self.listenTo(self.model, 'change', self.render, this);
			},

			render: function() {
				return this.$el.html(this.template(this.model.toJSON()));
			},

			re_render: function() {
				return this.$el.html(this.template({'results': this.model}));
			},

			collection_render: function() {
			}
		});
	});
