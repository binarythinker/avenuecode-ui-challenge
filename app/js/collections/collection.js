define(['jquery', 'underscore', 'backbone', 'model', 'currentView', 'collection', 'navView'], function($, _, Backbone, model, currentView, collection, navView) {
	var collection = Backbone.Collection.extend({

		model: model,

		url: "https://randomuser.me/api/?results=10",

		filterByGender: function(results, gender) {
			return getGender = _.filter(results, function(json) {
				return json.gender == gender;
			});
		}
	 });
	return collection;
});
