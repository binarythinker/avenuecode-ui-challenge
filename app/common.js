require.config ({
		baseUrl: 'app',
    paths: {
        jquery: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min',
        underscore: 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min',
        backbone: 'https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.3.3/backbone-min',

        /* Model */
        model: 'js/models/model',
        ageModel: 'js/models/ageModel',

        /* View */
        navView: 'js/views/navView',
        currentView: 'js/views/currentView',
        formView: 'js/views/formView',
        collectionView: 'js/views/collectionView',

				/* Collection */
				collection: 'js/collections/collection',

				/* Routes */
				router: 'js/routes/routes'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    }
});

define(['collectionView', 'collection', 'router'], 
	function(collectionView, collection, router) {
		new router;
		Backbone.history.start();
		new collectionView({
			model: new collection 
		});
	});
